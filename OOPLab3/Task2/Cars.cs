﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Cars
    {
        public delegate void CarInfo(string message);
        public event CarInfo CarMessage;
        public string Car;

        public Cars(string text)
        {
            Car = text;
        }

        public void ChangeCar(string text)
        {
            Car = text;
            if (CarMessage != null)
                CarMessage($"--Car Name сhanged--  New car name: {text}");
        }

        public void Print()
        {
            Console.WriteLine($"Car name is {Car}");
        }

        public void Error()
        {
            if (CarMessage != null)
                CarMessage($"--Name not сhanged--  Car name: {this.Car}");
        }
    }
}
