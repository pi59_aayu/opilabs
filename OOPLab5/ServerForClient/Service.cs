﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ServerForClient
{
    class Service : IContract
    {
        public void Say(string text ,string name)
        {
            Console.WriteLine($"Message from {name} [{text}]");
        }

        public void Connect(string text)
        {
            Console.WriteLine($"{text} connected to the server");
        }

        public void Disconnect(string text)
        {
            Console.WriteLine($"{text} disconnected to the server");
        }

        public List<string> GetProcess()
        {
            return Process.GetProcesses().Select(process => process.ProcessName).ToList();
        }

        public void CloseProcess(string text)
        {
            if (text != "")
            {
                var proc = Process.GetProcessesByName(text)[0];
                proc.Kill();
            }
           
        }

        public void OpenProcess(string text)
        {
            Process.Start(text);
        }

        public Bitmap TakeScreen()
        {
            Bitmap bitmap0 = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height, PixelFormat.Format32bppArgb);
            Graphics graphics0 = Graphics.FromImage(bitmap0);
            graphics0.CopyFromScreen(Screen.PrimaryScreen.Bounds.X, Screen.PrimaryScreen.Bounds.Y, 0, 0, Screen.PrimaryScreen.Bounds.Size, CopyPixelOperation.SourceCopy);
            MemoryStream ms = new MemoryStream();
            bitmap0.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
            return bitmap0;
        }

        public void ComputerOff()
        {
            Process.Start("shutdown", "/s /t 0");
        }

        public void ComputerReload()
        {
            Process.Start("shutdown", "/r /t 0");
        }
        public void ComputerSuspend()
        {
            Process.Start("shutdown", "/h /f");
        }
    }

}
