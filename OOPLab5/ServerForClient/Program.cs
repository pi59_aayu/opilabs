﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
 
namespace ServerForClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Uri address;
            BasicHttpBinding binding;
            Type contract;
            ServiceHost host;

                address = new Uri("http://localhost:4000/IContract");
                binding = new BasicHttpBinding();
                contract = typeof(IContract);
                host = new ServiceHost(typeof(Service));
                host.AddServiceEndpoint(contract, binding, address);
                host.Open();
               
            Console.WriteLine("-->Server is started<--");
            Console.ReadLine();
        }
    }
}
