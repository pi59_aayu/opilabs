﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
   
    public partial class Form1 : Form
    {
        List<string> work = new List<string>();
        string proc;
        string addressto;
        Uri address;
        BasicHttpBinding binding;
        EndpointAddress endpoint;
        ChannelFactory<IContract> factory;
        IContract channel;

        public Form1()
        {
            InitializeComponent();
            addressto = "http://localhost:4000/IContract";
            address = new Uri(addressto);
            binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = int.MaxValue;
            endpoint = new EndpointAddress(address);
            factory = new ChannelFactory<IContract>(binding, endpoint);
            channel = factory.CreateChannel();
            textBox2.Text = addressto;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox1.Image = Image.FromFile("C:/Users/Anton/source/repos/OOPLab5/Client/img/1.png");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
          
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try { 
            channel.Say(textBox1.Text, SystemInformation.UserName);
            }
            catch
            {
                MessageBox.Show("Server not found");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                endpoint = new EndpointAddress(address);
                factory = new ChannelFactory<IContract>(binding, endpoint);
                channel = factory.CreateChannel();
                channel.Connect(SystemInformation.UserName);
            }
            catch
            {
                MessageBox.Show("Server not found");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
            addressto = textBox3.Text;
            address = new Uri(addressto);
            textBox2.Text = textBox3.Text;
            textBox3.Text = "";
            }
            catch
            {
                MessageBox.Show("Invalid address");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                channel.Disconnect(SystemInformation.UserName);
                factory.Abort();
            }
            catch
            {

            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                channel.Disconnect(SystemInformation.UserName);
                factory.Abort();
            }
            catch
            {

            }

        }
        
        public void Update()
        {
            foreach (var item in channel.GetProcess())
            {
                listBox1.Items.Add(item);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try { 
            Update();
            }
            catch
            {
                MessageBox.Show("Server not found");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try { 
            channel.OpenProcess("mspaint");
            Update();
            }
            catch
            {
                MessageBox.Show("Server not found");
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try { 
            channel.OpenProcess("vlc");
            Update();
            }
            catch
            {
                MessageBox.Show("Server not found");
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try { 
            channel.OpenProcess("notepad");
            Update();
            }
            catch
            {
                MessageBox.Show("Server not found");
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try { 
            channel.OpenProcess("WINWORD");
            Update();
            }
            catch
            {
                MessageBox.Show("Server not found");
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            try { 
            channel.OpenProcess("calc");
            Update();
            }
            catch
            {
                MessageBox.Show("Server not found");
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            proc = listBox1.SelectedItem.ToString();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            try { 
            channel.CloseProcess(proc);
            textBox4.Text = "";
            Update();
            }
            catch
            {
                MessageBox.Show("Server not found");
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            try { 
            work.Clear();
            foreach (var item in listBox1.Items)
            {
                string demo = item.ToString();
                if (demo.StartsWith(textBox4.Text))
                {
                    work.Add(demo);
                }
            }

            listBox1.Items.Clear();
            foreach (var item in work)
            {
                listBox1.Items.Add(item);
            }
            if (textBox4.Text == "")
            {
                Update();
            }
            }
            catch
            {
                MessageBox.Show("Server not found");
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            try { 
            Bitmap screen = channel.TakeScreen();
            pictureBox1.Image = screen;
            }
            catch
            {
                MessageBox.Show("Server not found");
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button13_Click(object sender, EventArgs e)
        {
            try { 
            channel.ComputerReload();
            }
            catch
            {
                MessageBox.Show("Server not found");
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            try { 
            channel.ComputerOff();
            }
            catch
            {
                MessageBox.Show("Server not found");
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            try { 
            channel.ComputerSuspend();
            }
            catch
            {
                MessageBox.Show("Server not found");
            }
        }
    }
}
