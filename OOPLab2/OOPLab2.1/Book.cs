﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPLab2
{
    class Book
    {
        public string Name;
        public string Auhtor;
        public string Publics;

        public Book(string name, string auhtor, string publics)
        {
            this.Name = name;
            this.Auhtor = auhtor;
            this.Publics = publics;
        }

        public override string ToString()
        {
            return $"{Name} {Auhtor} {Publics}";
        }
    }
}
