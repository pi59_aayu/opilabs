﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPLab2._2
{
    class Class
    {

        public static double calculate(double x)
        {
            var a = x / 5.0;
            Console.WriteLine(a);
            return (a);
        }

        public double calculateY(double y)
        {
            var a = Math.Pow(y, 2) + 5;
            Console.WriteLine(a);
            return a;
        }
    }

    static class Demo
    {
       public static double calculate2(double x)
        {
            var a = Math.Pow(Math.E, x);
            Console.WriteLine(a);
            return a;
        }
    }
}
