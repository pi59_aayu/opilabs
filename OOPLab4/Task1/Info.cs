﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOPLab4
{
    public partial class Info : Form
    {
        string Name;
        long Size;
        double Time;
        public Info(string name, long size, double time)
        {
            InitializeComponent();
            Name = name;
            Size = size;
            Time = time;

        }

        private void Info_Load(object sender, EventArgs e)
        {
          
        }

        private void Info_Shown(object sender, EventArgs e)
        {
            textBox1.Text = Name;
            textBox2.Text = Size.ToString() + "b";
            textBox3.Text = Time.ToString() + "ms";
        }
    }
}
