﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;
using System.Diagnostics;

namespace OOPLab4
{
    public partial class Form1 : Form
    {
        static SymmetricAlgorithm aes = new AesManaged();
        string password = "demo";
        FileEditor FileInst;
        string name;
        double size;
        double time;
        public BackgroundWorker AsyncWorker;
        public Form1()
        {
            InitializeComponent();
            FileInst = new FileEditor();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                FileInst.Addfiles(openFileDialog1);
                textBox1.Text = openFileDialog1.FileName;
                progressBar1.Value = 0;
                label2.Text = "0";
                name = openFileDialog1.FileName;
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            AsyncWorker.DoWork -= AsyncWorker_DoWork2;
            AsyncWorker.DoWork += AsyncWorker_DoWork;
            AsyncWorker.RunWorkerAsync();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AsyncWorker.DoWork -= AsyncWorker_DoWork;
            AsyncWorker.DoWork += AsyncWorker_DoWork2;
            AsyncWorker.RunWorkerAsync();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            AsyncWorker = new BackgroundWorker();
            AsyncWorker.ProgressChanged += AsyncWorker_ProgressChanged;
            AsyncWorker.WorkerReportsProgress = true;
            AsyncWorker.WorkerSupportsCancellation = true;
            textBox2.Text = password;
        }
        public void AsyncWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Stopwatch StartTime = new Stopwatch();
            StartTime.Start();
            AsyncWorker.RunWorkerCompleted += AsyncWorker_Completed;
            var filesToEncrypt = new List<FileInfo>();
            filesToEncrypt.Add(FileInst.obj[0]);
            string text = "";
            using (var reader = new StreamReader(FileInst.obj[0].FullName))
                text = reader.ReadToEnd();

            double a = -100000000, b = 100000000;
            int oldPercent = 0;
            for (double x = a; x <= b; x += 1)
            {
                int percent = (int)((x - a) / (b - a) * 100);
                if (percent != oldPercent)
                    AsyncWorker.ReportProgress(percent);
                if (AsyncWorker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                oldPercent = percent;
            }

            FileEditor.EncryptData(aes, text, FileInst.obj[0].FullName);
            MessageBox.Show("Файл зашифровано!");
            StartTime.Stop();
            time = StartTime.Elapsed.TotalMilliseconds;
            AsyncWorker.CancelAsync();
          
        }

        public void AsyncWorker_DoWork2(object sender, DoWorkEventArgs e)
        {
            AsyncWorker.RunWorkerCompleted -= AsyncWorker_Completed;
            var filesToDecrypt = new List<FileInfo>();
            filesToDecrypt.Add(FileInst.obj[0]);

            double a = -100000000, b = 100000000;
            int oldPercent = 0;
            for (double x = a; x <= b; x += 1)
            {
                int percent = (int)((x - a) / (b - a) * 100);
                if (percent != oldPercent)
                    AsyncWorker.ReportProgress(percent);
                if (AsyncWorker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                oldPercent = percent;
            }
           
            foreach (var item in filesToDecrypt)
                FileEditor.DecryptData(aes, item.FullName);
            MessageBox.Show("Файл розшифровано");
            AsyncWorker.CancelAsync();

        }
        public void AsyncWorker_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            Info Show = new Info(name, FileInst.GetSize(), time);
            Show.Show();

        }
        public void AsyncWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
            label2.Text = e.ProgressPercentage.ToString();
        }

        public void GenerateKey()
        {
            byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(password);
            passwordBytes.CopyTo(aes.Key,0);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            password = textBox3.Text;
            GenerateKey();
            textBox3.Clear();
            textBox2.Text = password;
        }
    }
    
}

